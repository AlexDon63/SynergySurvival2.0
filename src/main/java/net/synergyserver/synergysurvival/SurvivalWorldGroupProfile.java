package net.synergyserver.synergysurvival;

import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Transient;

/**
 * Represents the profile of a player in the Lobby world group.
 */
@Entity(value = "worldgroupprofiles")
public class SurvivalWorldGroupProfile extends WorldGroupProfile {

    @Property("d")
    private int deaths;
    @Property("ld")
    private long lastDeath;

    @Transient
    private DataManager dm = DataManager.getInstance();
    @Transient
    private long lastDamage;
    @Transient
    private long enteredSun;
    @Transient
    private long lastSunDamage;

    /**
     * Required constructor for Morphia to work.
     */
    public SurvivalWorldGroupProfile() {}

    /**
     * Creates a new <code>SurvivalWorldGroupProfile</code> with the given parameters.
     *
     * @param wgp The <code>WorldGroupProfile</code> to extend.
     */
    public SurvivalWorldGroupProfile(WorldGroupProfile wgp) {
        super(wgp.getWorldGroupName(), wgp.getPlayerID(), wgp.getLastLogIn(), wgp.getLastLogOut(),
                wgp.getLastLocation(), wgp.getPrefID());
    }

    /**
     * Gets the number of deaths experienced in this world group.
     *
     * @return The number of deaths.
     */
    public int getDeaths() {
        return deaths;
    }

    /**
     * Sets the number of deaths experienced in this world group.
     *
     * @param deaths The new number of deaths.
     */
    public void setDeaths(int deaths) {
        this.deaths = deaths;
        dm.updateField(this, SurvivalWorldGroupProfile.class, "d", deaths);
    }

    /**
     * Gets the timestamp of the last death in this world group.
     *
     * @return The last time the player died.
     */
    public long getLastDeath() {
        return lastDeath;
    }

    /**
     * Sets the timestamp of the last time the player died.
     *
     * @param lastDeath The new timestamp.
     */
    public void setLastDeath(long lastDeath) {
        this.lastDeath = lastDeath;
        dm.updateField(this, SurvivalWorldGroupProfile.class, "ld", lastDeath);
    }

    /**
     * Gets the timestamp of when the player last took damage.
     *
     * @return The last time the player took damage.
     */
    public long getLastDamage() {
        return lastDamage;
    }

    /**
     * Sets the timestamp of the last time the player took damage.
     *
     * @param lastDamage The new timestamp.
     */
    public void setLastDamage(long lastDamage) {
        this.lastDamage = lastDamage;
    }

    /**
     * Gets the timestamp of when the player entered the sun if they're
     * currently in sun. If the player isn't in sun then this returns -1.
     *
     * @return The time the player entered sun.
     */
    public long getEnteredSun() {
        return enteredSun;
    }

    /**
     * Sets the timestamp of when the playered entered the sun if they're
     * currently in sun. IF the player isn't in sun then set to -1.
     *
     * @param enteredSun The new timestamp.
     */
    public void setEnteredSun(long enteredSun) {
        this.enteredSun = enteredSun;
    }

    /**
     * Gets the timestamp of when the player last took sun damage.
     *
     * @return The last time the player took sun damage.
     */
    public long getLastSunDamage() {
        return lastSunDamage;
    }

    /**
     * Sets the timestamp of when the player last took sun damage.
     *
     * @param lastSunDamage The new timestamp.
     */
    public void setLastSunDamage(long lastSunDamage) {
        this.lastSunDamage = lastSunDamage;
    }
}
