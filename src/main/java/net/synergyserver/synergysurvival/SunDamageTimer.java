package net.synergyserver.synergysurvival;

import net.synergyserver.synergycore.WeatherType;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.ItemUtil;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergycore.utils.TimeUtil;
import net.synergyserver.synergycore.utils.WorldUtil;
import net.synergyserver.synergysurvival.settings.SurvivalTieredMultiOptionSetting;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class SunDamageTimer {

    public static Runnable sunDamageTimer = () -> {
        for (Player p : Bukkit.getOnlinePlayers()) {
            MinecraftProfile mcp = PlayerUtil.getProfile(p);

            // Ignore the player if the current WGP is not a survival WGP
            if (!(mcp.getCurrentWorldGroupProfile() instanceof SurvivalWorldGroupProfile)) {
                continue;
            }

            // Ignore the player if their difficulty isn't high enough to be affected by the sun
            if (!SurvivalTieredMultiOptionSetting.DIFFICULTY.isValueAtLeast(mcp, "quite_easy")) {
                continue;
            }

            SurvivalWorldGroupProfile wgp = (SurvivalWorldGroupProfile) PlayerUtil.getProfile(p).getCurrentWorldGroupProfile();
            World world = p.getWorld();

            // Ignore the player if it's night and reset their entered sun time
            if (world.getTime() > 7700L) {
                wgp.setEnteredSun(-1L);
                continue;
            }

            Location loc = p.getLocation();

            // Ignore the player if they're not in the overworld or in clear weather and reset their entered sun time
            if (!world.getEnvironment().equals(World.Environment.NORMAL) || !WorldUtil.getWeather(loc).equals(WeatherType.CLEAR)) {
                wgp.setEnteredSun(-1L);
                continue;
            }

            // Ignore the player if they're wearing a helmet and reset their entered sun time
            if (p.getInventory().getHelmet() != null && !ItemUtil.isAir(p.getInventory().getHelmet().getType())) {
                wgp.setEnteredSun(-1L);
                continue;
            }

            Block highestBlock = (new Location(world, loc.getBlockX(), world.getHighestBlockYAt(p.getLocation()) - 1, loc.getBlockZ())).getBlock();
            if (ItemUtil.isAir(highestBlock.getType()) || highestBlock.getY() - 1 < p.getLocation().getBlockY()) {

                // If the highest block is air or below the player, then they are in the sun

                long delay = TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.SECOND, TimeUtil.TimeUnit.MILLI,
                        PluginConfig.getConfig(SynergySurvival.getPlugin()).getLong("sun_damage.damage_delay_seconds"));
                if (wgp.getEnteredSun() == -1L) {
                    // If they just entered the sun then set the entered timestamp to now
                    wgp.setEnteredSun(System.currentTimeMillis());
                } else if (System.currentTimeMillis() - wgp.getEnteredSun() > delay) {
                    // If they have been in the sun for longer than the configured delay then do damage to them
                    long interval = TimeUtil.toFlooredUnit(TimeUtil.TimeUnit.SECOND, TimeUtil.TimeUnit.MILLI,
                            PluginConfig.getConfig(SynergySurvival.getPlugin()).getLong("sun_damage.damage_interval_seconds"));
                    if (!p.isDead() && System.currentTimeMillis() - wgp.getLastSunDamage() > interval) {
                        // Damage the player by half a heart
                        if (p.getHealth() > 1) {
                            p.damage(1);
                        } else {
                            PlayerUtil.killPlayer(p, "death_message.sun");
                        }

                        wgp.setLastSunDamage(System.currentTimeMillis());
                    }
                }
            } else {
                // Since the player went indoors reset their entered sun time
                wgp.setEnteredSun(-1L);
            }
        }
    };
}
